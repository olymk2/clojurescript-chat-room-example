(ns cards.chat
  (:require
   [chatty.chat :as chat]
   [reagent.core :as reagent]
   [devcards.core :refer-macros [defcard-rg defcard]]))

(defcard-rg test-chat-icon
  "Sample widget displaying reading"
  (fn [data-atom _]
    (chat/icon))
  {:test "123"}
  {:inspect-data true})

(defcard-rg test-chat-box
  "Sample widget displaying reading"
  (fn [data-atom _]
    (chat/chat-box))
  {}
  {:inspect-data true})

(defcard-rg test-contact-box
  "Sample widget displaying reading"
  (fn [data-atom _]
    (chat/contact-list data-atom))
  (reagent/atom [{:name "bert" :number "123"}
                 {:name "ralf" :number "456"}
                 {:name "very very long username here lets see what happens" :number "8746987013479265586"}])
  {:inspect-data true})

(defcard-rg test-messages-box
  "Sample widget displaying reading"
  (fn [data-atom _]
    [chat/msg-list data-atom])
  (reagent/atom [{:msg "hello" :author "bert" :number "123"}
                 {:msg "goodbye" :author "ralf" :number "456"}])
  {:inspect-data true})



