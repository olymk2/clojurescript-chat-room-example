(ns cards.core
  (:require
   [devcards.core]
   [cards.chat :as cards_chat]
   [chatty.chat :as chat]
   [chatty.core :as chatcore]
   [reagent.core :as reagent :refer [atom]]
   [devcards.core :refer-macros [defcard-rg defcard]]))

(devcards.core/start-devcard-ui!)

(defcard-rg draw-item-list
  "Sample widget displaying reading"
  (fn [data-atom _]
    (chat/item-list "List Title" data-atom))
  (atom ["Item 1" "Item 2"])
  {:inspect-data true})


(defn user-badge [user-data]
  [:div.bg-black.flex.items-center.w5.br3.ph2.pv2.justify-between
   [:div.w6.ph-4.white.ph2.mv1.dib (:displayname user-data)]
   [:div
   [:img.h2.w2.dib.justify-end {:src (:avatar user-data)}]
   [:a.f6.link.dim.ph3.pv2.mb2.white.bg-black
    {:href "#0"}
    [:i.fas.fa-chevron-down]]]])

(defcard-rg test-profile-badge
  "Sample widget displaying reading"
  (fn [data-atom _]
    (user-badge @data-atom))
  {:avatar "123" :displayname "@user"}
  {:inspect-data true})


(defcard-rg test-chat-box
  "Sample widget displaying reading"
  (fn [data-atom _]
    (reset! chat/app-show-chat true)
    (chat/chat-box))
  {:avatar "123" :displayname "@user"}
  {:inspect-data true})
