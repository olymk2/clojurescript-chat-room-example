(ns chatty.matrix
  (:require [ajax.core :refer [raw-response-format json-request-format url-request-format json-response-format GET POST PUT ajax-request]]))

(def url "https://matrix.org/_matrix/client/r0/")
(def ajax-defaults {;:format :json
                    :format (json-request-format)
                        ;:keywords? true
                        ;:response-format :json
                    :response-format (json-response-format {:keywords? true})})
(defn register [{:keys [username password auth] :as extra}])

(defn matrix-handler [response]
  (.log js/console response))

(defn matrix-access-token
  ([] {:access_token nil})
  ([token] {:access_token token}))

(defn login-request
  ([] {:uri (str url "login")})
  ([{:keys [username password] :as extra}]
   (-> {}
       (assoc :uri (str url "login"))
       (assoc :params {:user username
                       :password password
                       :type (or (:login-type :extra) "m.login.password")}))))

(defn login
  ([]
   (ajax-request
    (merge ajax-defaults
           {:method :get :handler matrix-handler}
           (login-request))))
  ([{:keys [username password] :as extra}]
   (ajax-request
    (merge ajax-defaults
           {:method :post :handler matrix-handler}
           (login-request extra)))))

(defn room-request [{:keys [room] :as extra}]
  (str url "createRoom")
  (-> {}
      (assoc :uri (str url "createRoom"))
      (assoc :url-params {:type (or (:login-type extra) "m.login.password")})
      (assoc :params {:room_alias_name room})))

(defn room
  ([{:keys [room] :as extra}]
   (ajax-request
    (merge ajax-defaults
           {:method :post :handler matrix-handler}
           (room extra)))))

(defn send-message [{:keys [room body] :as extra}]
  (str url "createRoom")
  (-> {}
      (assoc :url (str url "rooms/" room "/send/m.room.message"))
      (assoc :url-params {:type (or (:login-type extra) "m.login.password")})
      (assoc :params {:msgtype (or (:msg-type extra) "m.text")})))

(comment
  (let [room "!IvOJVHgQxKbWvwJxnt:matrix.org"]
    (login)
    (login {:username "@oly:matrix.org" :password ""})

    (room {:room room})
    (room {:room "tutorial"})
    (send-message)))
