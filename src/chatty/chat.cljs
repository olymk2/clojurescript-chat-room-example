(ns chatty.chat
  (:require [ajax.core :refer [raw-response-format GET POST PUT]]
            [goog.string :as gstring]
            [chatty.components :as c]
            [nano-id.core :as nano :refer [nano-id]]
            [reagent.core :as reagent :refer [atom as-element]]
            [reitit.frontend :as rf :refer [query-params]]
            [reitit.impl :as impl])
  (:import goog.Uri))

;; Create a load of state to manipulate
(defonce app-notifications (atom "No notifications"))
(defonce app-room-members (atom [{:name "bert" :number "123"}
                                 {:name "ralf" :number "456"}
                                 {:name "very very long username here lets see what happens" :number "8746987013479265586"}]))

(defonce app-room-messages (atom ()))
(defonce app-batch-from (atom ""))
(defonce app-batch-to (atom ""))
(defonce app-response-states (atom ""))
(defonce app-message (atom ""))
(defonce app-show-chat (atom false))
(defonce app-show-view (atom true))

(defonce matrix-consent-given (atom nil))
(defonce matrix-consent-message (atom nil))
(defonce matrix-access-token (atom ""))
(defonce matrix-username (atom ""))
(defonce matrix-password (atom ""))

(defonce matrix-connected (atom nil))
(defonce matrix-room (atom "!IvOJVHgQxKbWvwJxnt:matrix.org"))
(defonce matrix-room-alias (atom nil))

(defonce matrix-url "https://matrix.org/_matrix/client/r0")
(defonce matrix-consent-url (str "https://matrix.org/_matrix/consent"))
(defonce matrix-login-url (str matrix-url "/login"))
(defonce matrix-profile-url (str matrix-url "/profile/%s"))
(defonce matrix-register-url (str matrix-url "/register"))
(defonce matrix-sync-url (str matrix-url "/sync"))
(defonce matrix-join-room-url (str matrix-url "/rooms/%s/join"))
(defonce matrix-room-messages-url (str matrix-url "/rooms/%s/messages"))
(defonce matrix-room-state-url (str matrix-url "/rooms/%s/state"))
(defonce matrix-send-msg-url (str matrix-url "/rooms/%s/send/m.room.message/%s"))

(defn error-handler [{:keys [status status-text response]}]
  (.log js/console response)
  (if (not-empty (:consent_uri response))
    (reset! matrix-consent-given (:consent_uri response)))
  (.log js/console @matrix-consent-given)

  (reset! app-notifications (:error response)))

(defonce ajax-defaults {:format :json
                        :keywords? true
                        :response-format :json :error-handler error-handler})

(defn url-params-to-map [url]
  (query-params (.parse Uri url)))

(defn make-form-body []
  (let [params (url-params-to-map @matrix-consent-given)]
    (let [form-data (doto
                     (js/FormData.)
                      (.append "v" "1.0")
                      (.append "u" (:u params))
                      (.append "h" (:h params)))]
      (.log js/console form-data)
      form-data)))

(defn handle-matrix-object [data]
  (case (:type data)
    "m.room.message"
    (swap! app-room-messages conj
           {:msg (get-in data [:content :body] "msg")
            :author (:sender data)})
    "m.room.name"
    (reset! matrix-room-alias (get-in data [:content :name]))
    "m.room.member"
    (swap! app-room-members conj
           {:name (:displayname (:content data))
            :fqdn (:sender data)
            :image (:avatar_url (:content data))})
    (.log js/console (str (:type data) " currently not handled " (str data)))))

(defn matrix-register-user [username password]
  (POST (str matrix-login-url)
    (GET (str matrix-sync-url)
      (assoc
       ajax-defaults
       :params {:user username :password password}
       :handler (fn [response]
                  (reset! matrix-access-token (:access_token response)))))))

(defn matrix-sync []
  (GET (str matrix-sync-url)
    (assoc
     ajax-defaults
     :url-params {:full_state true :access_token @matrix-access-token}
     :handler
     (fn [response]
       (.log js/console  response)
       (reset! app-response-states ())
       (reset! app-batch-to (:next_batch response))
       (reset! app-batch-to "")
       (reset! app-batch-from
               (get-in response
                       [:rooms :join (keyword @matrix-room) :timeline :prev_batch]
                       (:next_batch response)))
       (doall (map (fn [item]
                     (handle-matrix-object item)
                     (swap! app-response-states conj (:type item))) response))))))


(defn matrix-get-message []
  (GET (gstring/format matrix-room-messages-url @matrix-room)
    (assoc
     ajax-defaults
     :url-params {:dir "f"
                  :limit "100"
                  :from @app-batch-from
                  :to @app-batch-to
                  :access_token @matrix-access-token}
     :handler (fn [response]
                (doall (map (fn [item]
                              (handle-matrix-object item))
                            (:chunk response)))))))

(defn matrix-get-media-messages []
  (matrix-get-messages))


(defn matrix-consent [url]
  (let [params (url-params-to-map @matrix-consent-given)]
    (POST matrix-consent-url
      :body (make-form-body)
      :params {:v "1.0"
               :u (:u params)
               :h (:h params)}
      :handler
      (fn [response]
        (reset! matrix-consent-message response)))))

(defn matrix-join-room []
  (POST (gstring/format matrix-join-room-url @matrix-room)
    (assoc
     ajax-defaults
     :url-params {:access_token @matrix-access-token}
     :handler
     (fn [response]
       (reset! matrix-room-alias (clojure.string/replace @matrix-room #":matrix.org" ""))
       (matrix-sync)))))

(defn matrix-register-guest []
  (POST (str matrix-register-url "?kind=guest")
    (assoc
     ajax-defaults
     :params {:kind "guest"}
     :handler (fn [response]
                (reset! matrix-access-token (:access_token response))
                (matrix-join-room)))))

(defn matrix-send-msg [room msg]
  (PUT (gstring/format matrix-send-msg-url room (nano-id))
    (assoc
     ajax-defaults
     :url-params {:access_token @matrix-access-token}
     :params {:msgtype "m.text" :body msg}
     :handler (fn [response]
                (matrix-get-message)))))

(defn matrix-room-state []
  (GET (gstring/format matrix-room-state-url @matrix-room)
    (assoc
     ajax-defaults
     :url-params {:access_token @matrix-access-token}
     :handler  (fn [response]
                 (reset! app-response-states ())
                 (doall (map (fn [item]
                               (swap! app-response-states conj (:type item))
                               (handle-matrix-object item))
                             response))))))
;; components
(defn button [text callable]
  [:button.mr-2.bg-blue-500.hover:bg-blue-700.text-white.font-bold.py-2.px-4.rounded
   {:on-click #(callable)} text])

(defn username-input []
  (fn []
    [:form.m-4
     {:on-submit (fn [e] (.preventDefault e) (matrix-send-msg @matrix-room @app-message))}
     [:div.flex.flex-row.mb-4
      [:div.flex.shrink.pr-4
       [:label.flex-col   [:i.far.fa-keyboard]]]
      [:div.flex-auto

       [:input.w-full {:placeholder "Type message here" :defaultValue @app-message
                       :on-change (fn [e] (reset! app-message (-> e .-target .-value)))}]]]
     [:button.mr-2.bg-blue-500.hover:bg-blue-700.text-white.font-bold.py-2.px-4.rounded "Send"]]))

(defn item-list [title list]
  (fn []
    [:ul.bg-blue-100
     (doall
      (for [[index item] (map-indexed vector @list)]
        ^{:key index}
        [:li.p-1 item]))]))

(defn chat-messages [title list]
  (fn []
    [:div.px-2.text-lg.bg-blue-200.p-2 title
     [item-list list]]))

(defn icon []
  [:div.absolute.bottom-0.left-0.m-10
   [:div.px-2.text-lg.bg-blue-300.p-2 {:on-click #(swap! app-show-chat not)}
    [:div.db.dtc-ns.v-mid-ns [:i.far.fa-comments.f1]]
    [:div.db.dtc-ns.v-mid.ph2.pr0-ns.pl3-ns [:p.lh-copy "Chat to us"]]]])

(defn msg [data]
  [:<>
   [:a.db.pv2.ph3.ph0-l.no-underline.black.dim {:href ""}
    [:div.flex.flex-column.flex-row-ns
     [:div.w-100.pl3-ns
      [:p.f6.f5-l.lh-copy (:author data)]
      [:p.f6.lh-copy.mv0 (:msg data)]]]]])

(defn msg-list [title messages]
  [:div.component
   [:section.mw7.center.avenir.overflow-y-auto.h5
    title
    (doall
     (for [[index item] (map-indexed vector @messages)]
       ^{:key index}
       [:article..bt.bb.b--black-10 (msg item)]))]])

(defn contact [data]
  [:<>
   (if (:image data)
     [:img.w2.h2.w3-ns.h3-ns.br-100 {:src (str "https://matrix.org/_matrix/media/v1/thumbnail/" (subs (:image data) 6) "?width=64&height=64&method=crop")}]
     [:i.fas.fa-user {:on-click #(reset! app-show-view true)}])
   [:div.pl3.flex-auto
    [:span.f6.db.black-70 (:name data)]
    [:span.f6.db.black-70 (:fqdn data)]
    [:div [:a.f6.link.blue.hover-dark-gray {:href "tel:"} (:number data)]]]])

(defn contact-list [contacts]
  [:ul.list.pl0.mt0.measure.center.overflow-y-auto.h5
   (c/profile {})
   (doall
    (for [[index item] (map-indexed vector @contacts)]
      ^{:key index}
      [:li.flex.items-center.lh-copy.pa3.ph0-l.bb.b--black-10
       (contact item)]))])

(defn chat-buttons []
  [:div.w-100
   [:a.no-underline.near-white.bg-animate.bg-near-black.hover-bg-gray.inline-flex.items-center.ma2.tc.br2.pa2
    [:i.far.fa-comments {:on-click #(reset! app-show-view true)}]]
   [:a.no-underline.near-white.bg-animate.bg-near-black.hover-bg-gray.inline-flex.items-center.ma2.tc.br2.pa2
    [:i.far.fa-id-card {:on-click #(reset! app-show-view false)}]]
   [:a.no-underline.near-white.bg-animate.bg-near-black.hover-bg-gray.inline-flex.items-center.ma2.tc.br2.pa2.fr
    [:i.far.fa-window-close {:on-click #(swap! app-show-chat not)}]]])

(defn chat-box []
  (if @app-show-chat
    [:div.px-2.text-lg.bg-blue-200.p-2.w-90.h-75
     (chat-buttons)
     (c/room matrix-room-alias)
     [:div
      (if @app-show-view
        [:<>
         [:div {:on-click #(swap! app-show-chat not)}]
         [:div [msg-list "Messages" app-room-messages]]
         [c/message-input matrix-send-msg matrix-room]]
        [:<> (contact-list app-room-members)])]]
    (icon)))

(defn chat-box-position []
  [:div.chatty.absolute.bottom-0.left-0.m-10
   (chat-box)])

;; Track atom change and call function on change

(reagent/track! matrix-consent matrix-consent-given)

;;(js/setInterval #((if @matrix-connected matrix-sync) 30000))
