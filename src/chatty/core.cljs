(ns chatty.core
  (:require [accountant.core :as accountant]
            [ajax.core :refer [GET]]
            [chatty.chat :as chat]
            [chatty.components :as c]
            [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [reitit.frontend :as reitit]))

(enable-console-print!)

;; Setup your page routes here
(def router
  (reitit/router
   [["/" :index]
    ["/about" :about]]))

(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

(defn home-page []
  [:div @chat/app-notifications]
  [:div.flex.mb-4.m-4
   [:div
    [:h1.block.mt-2.text-lg.leading-tight.font-semibold.text-gray-900 "Home Page"]
    [:p.pb-8
     [:a {:href "https://matrix.org/docs/spec/client_server/latest.html#id143"}
      "Matrix Guest API end points"]
     [:span " | "]
     [:a
      {:target "_blank" :href "https://riot.im/app/#/room/!IvOJVHgQxKbWvwJxnt:matrix.org"}
      "Matrix Test Chat Room"]
     [:span " | "]
     [:a
      {:target "_blank" :href "https://riot.im/app/#/room/#clojurians:matrix.org"}
      "Matrix Chat Room"]]
    [:div.pb-8 [chat/button "Join" chat/matrix-join-room]
     [chat/button "Sync" chat/matrix-sync]
     [chat/button "Room State" chat/matrix-room-state]
     [chat/button "Room Messages" chat/matrix-get-message]
     [chat/button "Media Messages" chat/matrix-get-media-messages]]
    [c/message-input chat/matrix-send-msg chat/matrix-join-room chat/matrix-get-message]

    [:div.flex.flex-wrap.-mx-2.pb-8.overflow-hidden
     [:div.px-2.text-lg.bg-blue-200.p-2 "Info"
      [:ul.bg-blue-100
       [:li "Room " @chat/matrix-room]

       (if (not-empty @chat/matrix-consent-given)
         [:li "Consent " [:a {:href @chat/matrix-consent-given :target "_blank"} @chat/matrix-consent-given]])
       [:li "Batch from " @chat/app-batch-from]
       [:li "Batch to " @chat/app-batch-to]
       [:li "Token " @chat/matrix-access-token]
       [:li "consent " @chat/matrix-consent-given]]]]

    [:div [:h1 "Media"]
;;      https://matrix.org/_matrix/client/r0/rooms/!fmCpNwqgIiuwATlcdw%3Amatrix.org/messages
     
;;      ?from=t12658-1185452395_757269739_703741_396611841_242622736_1016457_37267460_19385305_65841
;;      &limit=20
;;      &dir=b
;; &filter={"lazy_load_members":true,"filter_json":{"contains_url":true,"types":["m.room.message"]},"types":["m.room.message"],"not_types":[],"rooms":null,"not_rooms":[],"senders":null,"not_senders":[],"contains_url":true}
    ]

    [:div.flex.flex-wrap.-mx-2
     [:div.pr-2 {:class "w-1/3"} [chat/item-list "Messages" chat/app-room-messages]]
     [:div.pr-2 {:class "w-1/3"} [chat/item-list "Members" chat/app-room-members]]
     [:div.pr-2 {:class "w-1/3"} [chat/item-list "States" chat/app-response-states]]]
    (chat/chat-box-position)
    [:a.navbar-item {:href (path-for :about)} "About Page"]]])

(defn about-page []
  (chat/matrix-sync)
  [:div
   [:div [:h1.m-4 "About Page"]
    [:p "Sample app, demoing using ajax calls and reagent to render the page with some simple route from reitit, basically enought to get you started on your clojurescript journey."]
    [:p "To use this as a template remove core.cljs and any references to it in core.cljs"]
    [:a.navbar-item {:href (path-for :index)} "Home Page"]]])

(defn page-for [route]
  "Map your route keys to pages here"
  (case route
    :index #'home-page
    :about #'about-page))

;; retit router
(defn current-page []
  (fn []
    (let [page (:current-page (session/get :route))]
      [:div.container.mx-auto [page]])))

(defn startup []
  (chat/matrix-register-guest)
  ;;(chat/matrix-register-guest @chat/matrix-username @chat/matrix-password)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [match (reitit/match-by-path router path)
            current-page (:name (:data  match))
            route-params (:path-params match)]
        (session/put! :route {:current-page (page-for current-page)
                              :route-params route-params})))

    :path-exists?
    (fn [path]
      (boolean (reitit/match-by-path router path)))})
  (accountant/dispatch-current!)

  (reagent/render [current-page] (.getElementById js/document "app")))

(startup)

