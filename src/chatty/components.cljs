(ns chatty.components)

(defn room [room]
  [:div.w-100
   (if @room
     [:span.bg-green.inline-flex.items-center.ma2.tc.br2.pa2
      {:title (str "Chatting in " @room)}
      [:i.fas.fa-check.w1.h1]]
     [:span.bg-red.inline-flex.items-center.ma2.tc.br2.pa2
      {:title "Not connected to a room"}
      [:i.fas.fa-exclamation.w1.h1]])
   [:span (if @room @room "Disconnected")]])

(defn profile [user]
  [:article.mw5.center.bg-white.br3.pa3.pa3-ns.mb2.ba.b--black-10
   [:div.tc
    (if (:avatar user)
      [:img.br-100.h3.w3.dib
       {:src "" :title "title text"}]
      [:i.fas.fa-user-edit.f1])
    [:h1.f4 (get :displayname user "Anonymous")]
    [:hr.mw3.bb.bw1.b--black-10]]])

(defn message-input [func room]
  (let [message (atom "")]
    (fn []
      [:form
       {:on-submit (fn [e] (.preventDefault e) (func @room @message))}
       [:div.flex.flex-row.mb-4.w-full
        [:div.flex-auto
         [:input.w-full.h2 {:placeholder "Type message here" :defaultValue @message
                            :on-change (fn [e] (reset! message (-> e .-target .-value)))}]]]
       [:button.mr-2.bg-blue-500.hover:bg-blue-700.text-white.font-bold.py-2.px-4.rounded  "Send " [:i.far.fa-comments.f4]]])))

(defn cards []
  [:article.bg-white.center.mw5.ba.b--black-10.mv4
   [:div.pv2.ph3
    [:h1.f6.ttu.tracked "Daily News Co."]]
   [:img.w-100.db {:src "http://tachyons.io/img/cat-720.jpg" :alt "Closeup photo of a tabby cat yawning."}]
   [:div.pa3 [:a.link.dim.lh-title {:href "#"} "15 things every cat owner should know"]
    [:small.gray.db.pv2 "AMP - " [:time "6 hours ago"]]]])
